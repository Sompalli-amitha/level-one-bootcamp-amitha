//WAP to find the volume of a tromboloid using 4 functions.
#include<stdio.h>
float input(char a)
{
    float n;
    printf("Enter the value of %c:\n",a);
    scanf("%f",&n);
    return n;
}
float  volume(float h,float d,float b)
{
    float volume=(((h*d*b)+(d/b))/3);
	return volume;
}
void output(float h,float d,float b,float v)
{
    printf("The volume of tromboloid of height %f,depth %f and breadth %f is:\n%f",h,d,b,v);
}
int main()
{
    float h,d,b,v;
	h=input('h');
	d=input('d');
	b=input('b');
	v=volume(h,d,b);
	output(h,d,b,v);
	return 0;
}