//WAP to find the sum of two fractions.

#include <stdio.h>

struct fraction
{
  int n;
  int d;
};
typedef struct fraction frac;
frac input();
frac sum(frac fract1, frac fract2);
int gcd(int p, int q);
int output(frac s);

int main()
{
  frac f1,f2,f3;
  printf("enter the 1st fraction\n");
  f1 = input();
  printf("enter the 2nd fraction\n");
  f2 = input();
  f3 = sum(f1,f2);
  output(f3);
  return 0;
}

frac input()
{
  frac f;
  printf("N : ");
  scanf("%d",&f.n);
  printf("D : ");
  scanf("%d",&f.d);
  return f;
}

int gcd(int p, int q)
{
  int i,a;
  for(i = 1; i <= p && i <= q; i++)
{
        if(p % i == 0 && q % i == 0)
         a = i;
}
    return a;
}

frac sum(frac fract1, frac fract2)
{
  frac fract3;
  int c;
  fract3.n = ((fract1.n*fract2.d)+(fract2.n*fract1.d));
  fract3.d = fract1.d*fract2.d;
  c = gcd(fract3.n,fract3.d);
  fract3.n = fract3.n/c;
  fract3.d = fract3.d/c;
  return fract3;
}

int output(frac s)
{
  printf("The sum of two given fraction  : %d / %d\n",s.n,s.d);
  return 0;
}